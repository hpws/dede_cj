# -*- coding: utf-8 -*-
# @Author: dapan 191042102
# @Date:   2019-11-21 09:52:38
# @Last Modified time: 2019-11-23 09:47:14

import  re
"""
rule = {
    'name'  : 'require|max:25',
    'age'   : 'number|between:1,120',
    'email' : 'email',
}
$data = {
    'name'  : 'thinkphp',
    'age'   : 121,
    'email' : 'thinkphp@qq.com',
}
$msg = {
    'name.require' : '名称必须',
    'name.max'     : '名称最多不能超过25个字符',
    'age.number'   : '年龄必须是数字',
    'age.between'  : '年龄必须在1~120之间',
    'email.email'        : '邮箱格式错误',
}
"""
class Validate():
    def __init__(self):
        self.rule = None
        self.msg = None
        self.error = None

    def check_value(self,value,key):
        rules = self.get_rule(self.rule[key])
        for i in rules:
            if type(i) is str:
                if i == 'require':
                    if not self.require(value):
                        return False,self.return_msg('require',key)
                if i == 'number':
                    if not self.number(value):
                        return False, self.return_msg('number', key)
                if i == 'email':
                    if not self.email(value):
                        return False, self.return_msg('email', key)
                if i == 'phone':
                    if not self.phone(value):
                        return False, self.return_msg('phone', key)
                if i == 'url':
                    if not self.url(value):
                        return False, self.return_msg('url', key)
            elif type(i) is list:
                if i[0] == 'max':
                    if not self.max(value,i[1]):
                        return False, self.return_msg('max', key)
                if i[0] == 'min':
                    if not self.min(value,i[1]):
                        return False, self.return_msg('min', key)
                if i[0] == 'between':
                    if not self.between(value,i[1]):
                        return False, self.return_msg('between', key)
        return True,''
    def return_msg(self,rule,key):
        temvar = "{}.{}".format(key,rule)
        if temvar in self.msg:
            remsg = self.msg[temvar]
        else:
            remsg = 'error'
        return  remsg

    def get_rule(self,rule):
        tem1 = rule.split("|")
        if len(tem1) > 1:
            for k,v in enumerate(tem1):
                tem2 = v.split(":")
                if len(tem2) > 1:
                    tem1[k] = tem2
        else:
            tem2 = tem1[0].split(":")
            if len(tem2) > 1:
                tem1[0] = tem2
        return tem1

    def require(self,value):
        if len(value)>0:
            return True
        else:
            return False

    def number(self,value):
        try:
            value = int(value)
            return True
        except Exception as e:
            return False

    def max(self,value,maxvalue):
        if type(value) is int:
            if value <= int(maxvalue):
                return True
        if type(value) is str:
            if len(value) <= int(maxvalue):
                return True
        return False

    def between(self,value,bet):
        betrule = bet.split(',')
        if type(value) is int and len(betrule)==2:
            if value >= int(betrule[0]) and value <= int(betrule[1]):
                return True
        return False

    def min(self,value,minvalue):
        if type(value) is int:
            if value >= int(minvalue):
                return True
        if type(value) is str:
            if len(value) >= int(minvalue):
                return True
        return False

    def email(self,value):
        p = re.compile('^[A-Za-z0-9\u4e00-\u9fa5]+@[a-zA-z0-9_-]+(\.[a-zA-Z0-9_-]+)+$')
        x = p.match(value)
        if x != None:
            return True
        return False

    def phone(self,value):
        p = re.compile('^1[3-9]\d{9}$')
        x = p.match(value)
        if x != None:
            return True
        return False

    def url(self,value):
        p = re.compile(r'^((https|http)?:\/\/)[^\s]+')
        x = p.match(value)
        if x != None:
            return True
        return False

    def getError(self):
        return self.error

    def check(self,data):
        try:
            for k,v in data.items():
                if k in self.rule.keys():
                    ischeck,msg = self.check_value(v,k)
                    if not ischeck:
                        self.error = msg
                        return False
            return True
        except Exception as e:
            print(e)
            return False