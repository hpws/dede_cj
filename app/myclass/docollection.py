import threading
from .. import fun
from ..models import Collectionlist
from ..models import Collection
from ..models import Article
from app.myclass.spider import spider
from app import create_app,db
app = create_app('default')
import time

class ConllectionListRun(threading.Thread):
    def __init__(self,id,row):
        threading.Thread.__init__(self)
        self.id = id
        self.r_list = row.rlist
        self.success = 0
        self.row = row
        self.charset = 'utf-8' if row.list_charset==1 else 'GB2312'
        self.curr = 0
        self.max = len(row.rlist)

    def run(self):
        try:
            with app.app_context():
                for i in self.r_list:
                    teplist = fun.get_list(i.url, self.row.reurlrex, self.row.retype, self.row.reurlrex1,self.charset)
                    if self.row.musthas:
                        teplist = fun.musthas(teplist, self.row.musthas)
                    if self.row.nothas:
                        teplist = fun.nothas(teplist, self.row.nothas)
                    if teplist:
                        for x in teplist:
                            tem = Collectionlist.query.filter_by(url=x).first()
                            if not tem:
                                db.session.add(Collectionlist(url=x, colid=self.id))
                        db.session.commit()
                        db.session.close()
                        self.curr = self.curr+1
                    time.sleep(0.5)
        except Exception as e:
            print(e)
            self.success = 0
        self.success = 1

    def get(self):
        if self.max>0:
            process = self.curr/self.max*100
            return {"process": process, "code": self.success}
        return {"process": 0, "code": 0}


class ConllectionShowRun(threading.Thread):
    def __init__(self,id,mydata,curllist):
        threading.Thread.__init__(self)
        self.id = id
        self.r_list = curllist
        self.success = 0
        self.row = {
            'charset': 'utf-8' if mydata.charset == 1 else 'GB2312',
            'match_title': mydata.match_title,
            'match_title_retype': mydata.match_title_retype,
            'trim_title': mydata.trim_title,
            'match_body': mydata.match_body,
            'match_body_retype': mydata.match_body_retype,
            'trim_body': mydata.trim_body,
            'match_page': mydata.match_page,
            'match_page_retype': mydata.match_page_retype,
        }
        self.typeid = mydata.typeid
        self.curr = 1
        self.max = len(curllist)
        self.currurl = ''

    def run(self):
        with app.app_context():
            myspider = spider(self.row)
            for i in self.r_list:
                self.currurl = i.url
                code, data = myspider.get_data(i.url)
                if code>0 and data:
                    db.session.add(Article(title=data['title'], content=data['content'], url=i.url, typeid=self.typeid))
                    db.session.query(Collectionlist).filter_by(url=i.url).update({'status': 2})
                elif code == -1:
                    db.session.query(Collectionlist).filter_by(url=i.url).update({'status': data})
                else:
                    db.session.query(Collectionlist).filter_by(url=i.url).update({'status': -1})
                db.session.commit()
                db.session.close()
                self.curr = self.curr + 1
                time.sleep(1)
        self.success = 1

    def get(self):
        if self.max>0:
            process = self.curr/self.max*100
            return {"process": process, "code": self.success,"curr":self.curr,"max":self.max,"currurl":self.currurl}
        return {"process": 100, "code": 1}