# -*- coding: utf-8 -*-
# @Author: dapan 191042102
# @Date:   2020-07-03 08:32:18
# @Last Modified time: 2020-07-14 09:47:48
from app.fun import *
import datetime
from app import create_app,db
from app.models import Addcountdb
import time

app = create_app('default')

class Addcount():
    def __init__(self):
    	self.now_date ="{}{}{}".format(datetime.datetime.now().year,datetime.datetime.now().month,datetime.datetime.now().day)
 			
    def delete(self):
    	with app.app_context():
            db.session.query(Addcountdb).filter(Addcountdb.adddate != self.now_date).delete()
            db.session.close()
    		

    def get(self,rid,adddate):
    	if rid:
    		if adddate == self.now_date:
	    		with app.app_context():
	    			count = db.session.query(Addcountdb).filter_by(rid=rid,adddate=self.now_date).count()
	    			return count
	    	else:
	    		self.delete()

	    		return 0



