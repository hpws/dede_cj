# -*- coding: utf-8 -*-
# @Author: dapan 191042102
# @Date:   2019-11-21 09:52:38
# @Last Modified time: 2020-10-26 10:06:43
from flask import render_template,request
from . import main
from .. import fun
from .. import db
import json
from ..models import Collection
from ..models import Collectionlist
from ..models import Referurl
from ..models import Arttype
from ..models import Article
from .formValidate import *
from app.myclass.spider import spider
from app.myclass.docollection import ConllectionListRun
from app.myclass.docollection import ConllectionShowRun
import requests
res = requests.session()
role_stats ={}
role_stats1 = {}


@main.route('/collection',endpoint='collection')
@fun.is_login
def collection():
  row = Collection.query.all()
  count = len(row)
  return render_template('collection/collection.html',row=row,count=count)


@main.route('/collection_add',endpoint='collection_add',methods=['GET','POST'])
@fun.is_login
def collection_add():
  if request.method == 'GET':
    category = Arttype.query.all()
    return render_template('collection/collection_add.html',category=category)
  elif request.method == 'POST':
    data = request.form
    Validate = val_Collection()
    if not Validate.check(data):
      return fun.msg_json(0, Validate.getError())
    urllist = data['urllist'].splitlines()
    for i in urllist:
      if not fun.is_url(i):
        return fun.msg_json(0, '网址错误！')
    Collectiondb = Collection(
      name=data['name'],
      list_charset=data['charset'],
      typeid=data['typeid'],
      retype=data['retype'],
      reurlrex=data['reurlrex'],
      reurlrex1=data['reurlrex1'],
      musthas=data['musthas'],
      nothas=data['nothas']
    )
    db.session.add(Collectiondb)
    for i in urllist:
      if fun.is_url(i):
        referurldb = Referurl(url=i,cid=Collectiondb.id)
        db.session.add(referurldb)
    db.session.commit()
    db.session.close()
    return fun.msg_json(1, '成功',Collectiondb.id)

@main.route('/collection_edit',endpoint='collection_edit',methods=['GET','POST'])
@fun.is_login
def collection_edit():
  id = request.args.get('id',0,type=int)
  if request.method == 'GET':
    if id:
      category = Arttype.query.all()
      row = Collection.query.filter_by(id=id).first()
      return render_template('collection/collection_edit.html',row=row,category=category)
  elif request.method == 'POST':
    data = request.form
    Validate = val_Collection()
    if not Validate.check(data):
      return fun.msg_json(0, Validate.getError())
    urllist = data['urllist'].splitlines()
    for i in urllist:
      if not fun.is_url(i):
        return fun.msg_json(0, '网址错误！')
    db.session.query(Collection).filter_by(id=id).update({
      'name'      : data['name'],
      'list_charset'   : data['charset'],
      'typeid'    : data['typeid'],
      'reurlrex'  : data['reurlrex'],
      'retype'    : data['retype'],
      'reurlrex1' : data['reurlrex1'],
      'musthas'   : data['musthas'],
      'nothas'    : data['nothas']
    })
    db.session.query(Referurl).filter_by(cid=id).delete()
    for i in urllist:
      if fun.is_url(i):
        referurldb = Referurl(url=i,cid=id)
        db.session.add(referurldb)
    db.session.commit()
    db.session.close()
    return fun.msg_json(1, '成功',id)

@main.route('/collection_getlist',endpoint='collection_getlist')
@fun.is_login
def collection_getlist():
  if request.method == 'GET':
    id = request.args.get('id',0,type=int)
    if id:
      row = Collection.query.filter_by(id=id).first()
      charset = 'utf-8' if row.list_charset==1 else 'GB2312'
      alist = []
      for i in row.rlist:
        teplist = fun.get_list(i.url, row.reurlrex, row.retype, row.reurlrex1,charset)
        if row.musthas:
          teplist = fun.musthas(teplist,row.musthas)
        if row.nothas:
          teplist = fun.nothas(teplist,row.nothas)
        if teplist:
          alist.append({
              'purl' : i.url,
              'curl' : teplist
          })
      return render_template('collection/collection_getlist.html',row=row,alist=alist)

@main.route('/collection_setarturl',endpoint='collection_setarturl',methods=['GET','POST'])
@fun.is_login
def collection_setarturl():
  id = request.args.get('id' , 0 ,type=int)
  if request.method == 'GET':
    row = Collection.query.filter_by(id=id).first()
    return render_template('collection/collection_setarturl.html',id=id,row=row)
  elif request.method == 'POST':
    data = request.form
    Validate = val_Collection1()
    if not Validate.check(data):
      return fun.msg_json(0, Validate.getError())
    db.session.query(Collection).filter_by(id=id).update({
      'match_page': data['match_page'],
      'charset'   : data['charset'],
      'match_page_retype': data['match_page_retype'],
      'match_title': data['match_title'],
      'match_title_retype': data['match_title_retype'],
      'trim_title': data['trim_title'],
      'match_body': data['match_body'],
      'match_body_retype': data['match_body_retype'],
      'trim_body': data['trim_body'],
    })
    db.session.commit()
    db.session.close()
    return fun.msg_json(1, '成功', data['yuurl'])

@main.route('/collection_show',endpoint='collection_show')
@fun.is_login
def collection_show():
  id = request.args.get('id',0,type=int)
  url = request.args.get('url','',type=str)
  if id and url:
    mydata = Collection.query.filter_by(id=id).first()
    row = {
      'charset':'utf-8' if mydata.charset==1 else 'GB2312',
      'match_title':mydata.match_title,
      'match_title_retype':mydata.match_title_retype,
      'trim_title':mydata.trim_title,
      'match_body':mydata.match_body,
      'match_body_retype':mydata.match_body_retype,
      'trim_body':mydata.trim_body,
      'match_page':mydata.match_page,
      'match_page_retype':mydata.match_page_retype,
    }
    myspider = spider(row)
    code, data =  myspider.get_data(url)
    if code>0 and data:
      title = data['title']
      content = data['content']
    else:
      title = ''
      content = ''
    return render_template('collection/collection_show.html',title=title,content=content,id=id)

@main.route('/collection_del/<id>',endpoint='collection_del')
@fun.is_login
def collection_del(id):
  if id:
    Collection.query.filter_by(id=id).delete()
    Referurl.query.filter_by(cid=id).delete()
    Collectionlist.query.filter_by(colid=id).delete()
    db.session.commit()
    db.session.close()
    return fun.msg_json(1, '删除成功')

@main.route('/start_collection',endpoint='start_collection')
@fun.is_login
def start_collection():
  id = request.args.get('id', 0, type=int)
  return render_template('collection/start_collection.html',id=id)

@main.route('/start_collectionshow',endpoint='start_collectionshow')
@fun.is_login
def start_collection():
  id = request.args.get('id', 0, type=int)
  return render_template('collection/start_collectionshow.html',id=id)


@main.route('/do_collection', endpoint='do_collection')
@fun.is_login
def do_collection():
  id = request.args.get('id', 0, type=int)
  role = request.args.get('role','',type=str)
  if id:
    try:
      row = Collection.query.filter_by(id=id).first()
      role_stats[role] = ConllectionListRun(id, row)
      role_stats[role].start()
      return fun.msg_json(1, '正在抓取')
    except Exception as e:
      print(e)
      return fun.msg_json(0, '文章网址抓取失败')
  return fun.msg_json(0, '文章网址抓取失败')

@main.route('/do_collection_show', endpoint='do_collection_show')
@fun.is_login
def do_collection_show():
  id = request.args.get('id', 0, type=int)
  role = request.args.get('role','',type=str)
  if id:
    mydata = Collection.query.filter_by(id=id).first()
    curllist = Collectionlist.query.filter_by(colid=id, status=1).all()
    role_stats1[role] = ConllectionShowRun(id, mydata,curllist)
    role_stats1[role].start()
    return fun.msg_json(1, '正在抓取')
    return '文章抓取完成'

@main.route('/do_collection_get',endpoint='do_collection_get')
@fun.is_login
def do_collection_get():
  role = request.args.get('role', '', type=str)
  if role:
    try:
      res = role_stats[role].get()
      return res
    except Exception as e:
      print(e)
      return {"process": 0, "code": 0}

@main.route('/do_collection_show_get',endpoint='do_collection_show_get')
@fun.is_login
def do_collection_get():
  role = request.args.get('role', '', type=str)
  if role:
    try:
      res = role_stats1[role].get()
      return res
    except Exception as e:
      print(e)
      return {"process": 0, "code": 0}


@main.route('/collectionlist',endpoint='collectionlist')
@fun.is_login
def collectionlist():
  colid = request.args.get('colid',0,type=int)
  page = request.args.get('page',1,type=int)
  status = request.args.get('status',0,type=int)
  kw = request.args.get('kw','',type=str)
  vue = request.args.get('vue', 0, type=int)
  per_page = 20
  where = []
  if kw:
    where.append(Collectionlist.url.like("%{}%".format(kw.strip())))
  if colid:
    where.append(Collectionlist.colid == colid)
  if status:
    where.append(Collectionlist.status == status)
  urllist = Collectionlist.query.filter(*where).join(Collection,Collectionlist.colid==Collection.id,isouter=True).order_by(Collectionlist.id.desc()).paginate(page,per_page=per_page,error_out=False)
  count = Collectionlist.query.filter(*where).count()
  data = enumerate(urllist.items)
  allpage = fun.getpagecount(per_page,count)
  pagination = urllist
  collist = Collection.query.all()
  if vue == 1:
    myjson = []
    for i in urllist.items:
      myjson.append(i.to_json())
    rejson = {'urllist':myjson,'all':allpage,'count':count}
    return json.dumps(rejson)
  else:
    return render_template('collection/collectionlist.html',pagination=pagination,data=data,collist=collist,kw=kw.strip(),colid=colid,page=page,status=status,count=count,all=allpage)


@main.route('/collectionlist_del',endpoint='collectionlist_del',methods=['GET','POST'])
@fun.is_login
def collectionlist_del():
  if request.method == 'GET':
    id = request.args.get('id',0,type=int)
    if id:
      Collectionlist.query.filter_by(id=id).delete()
      db.session.commit()
      db.session.close()
      return fun.msg_json(1, '已删除')
  elif request.method == 'POST':
    ids = request.form.get('data','',type=str)
    idsjs = json.loads(ids)
    for i in idsjs:
      Collectionlist.query.filter_by(id=i).delete()
    db.session.commit()
    db.session.close()
    return fun.msg_json(1, '已删除')