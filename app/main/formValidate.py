from app.myclass.Validate import Validate


class val_Collection(Validate):
    def __init__(self):
        self.rule = {
            'name': 'require|max:100',
            'charset':'number',
            'urllist': 'require',
            'reurlrex': 'require',
            'retype': 'number',
        }
        self.msg = {
            'name.require': '名称必须',
            'name.max': '名称最多不能超过100个字符',
            'charset.number':'页面编码格式错误',
            'urllist.require':'列表网址必须',
            'reurlrex.require':'文章网址匹配规则必须',
            'retype.number':'获取规则类型必须',
        }
        self.error = None

class val_Collection1(Validate):
    def __init__(self):
        self.rule = {
            'yuurl' : 'url',
            'match_title'  : 'require',
            'match_title_retype'  : 'number',
            'match_body'  : 'require',
            'match_body_retype'  : 'number',
        }
        self.msg = {
            'yuurl.url' :   '预览网址必填',
            'match_title.require' :   '文章标题匹配规则必填',
            'match_title_retype.number' :   '文章标题匹配方式必填',
            'match_body.require' :   '文章内容匹配规则必填',
            'match_body_retype.number' :   '文章内容匹配方式必填',
        }
        self.error = None