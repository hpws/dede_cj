from flask import render_template,request,session,redirect,url_for
from . import main
from .. import fun
from .. import db
from ..models import Admin
# from ..models import Collection
# from ..models import Release

# @main.before_app_request
# def before_app_request():
#   print("3343")

@main.route('/')
@fun.is_login
def index():
  title = "后台管理"
  return render_template('index.html',title=title)

@main.route('/welcome',endpoint='welcome')
@fun.is_login
def welcome():
  db.create_all()
  return render_template('welcome.html')

@main.route('/login',methods=['GET', 'POST'])
def login():
  if request.method == 'GET':
    return render_template('login.html')
  else:
    username = request.form['username']
    password = request.form['password']
    res = Admin.query.filter_by(username=username).first()
    if(res):
      if(fun.md5(fun.md5(password)) == res.password):
        session['user'] = res.username
        return fun.msg_json(1, '登录成功')
    return fun.msg_json(0, '账号密码错误')

@main.route('/logout')
def logout():
  session.pop('user', None)
  return redirect(url_for('main.login'))

# @main.route('/verify/index.html')
# def verify():
#   app = current_app._get_current_object()
#   appdir = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
#   codeurl = app.config['API_URL']+"/index.php/verify/index.html"
#   resp = res.get(codeurl)
#   con = resp.content
#   resp1 = Response(con,mimetype="image/jpeg")
#   return resp1
