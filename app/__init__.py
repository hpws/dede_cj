from flask import Flask
from config import config
from flask_sqlalchemy import SQLAlchemy


# bootstrap = Bootstrap()
db = SQLAlchemy()

def create_app(config_name):
  app = Flask(__name__)
  db.init_app(app)
  app.config.from_object(config[config_name])
  config[config_name].init_app(app)
  from .main import main as main_blueprint
  app.register_blueprint(main_blueprint)
  return app

