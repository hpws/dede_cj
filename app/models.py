from _pydecimal import Decimal

from sqlalchemy import DATETIME, DATE

from app import db
from datetime import datetime
import json

class Admin(db.Model):
	__tablename__ = 'admin'
	id = db.Column(db.Integer,primary_key=True)
	username = db.Column(db.String(20),unique=True,index=True)
	password = db.Column(db.String(50))
	ip = db.Column(db.String(20))
	addtime = db.Column(db.DateTime,default=datetime.now)
	lasttime = db.Column(db.DateTime, default=datetime.now,onupdate=datetime.now)
	def __init__(self, **kwargs):
		super(Admin, self).__init__(**kwargs)	
		pass
	def __repr__(self):
		return '<Admin %r>' % self.username


class Arttype(db.Model):
	__tablename__ = 'arttype'
	id = db.Column(db.Integer,primary_key=True)
	catname = db.Column(db.String(20))





class Article(db.Model):
	__tablename__ = 'article'
	id = db.Column(db.Integer,primary_key=True)
	title = db.Column(db.String(50),index=True)
	content = db.Column(db.Text)
	url = db.Column(db.String)
	addtime = db.Column(db.DateTime,default=datetime.now)
	lasttime = db.Column(db.DateTime, default=datetime.now,onupdate=datetime.now)
	status = db.Column(db.Integer,default=1)
	typeid = db.Column(db.Integer, db.ForeignKey('arttype.id'))
	status_ifg = db.Column(db.Integer,default=0)
	ifg_id = db.Column(db.String)
	ifg_userid = db.Column(db.String)
	arttype = db.relationship('Arttype', backref='art')

	def to_json(self):
		json_data = {
			'id': self.id,
			'title': self.title,
			'url': self.url,
			'addtime': self.addtime.strftime('%Y-%m-%d %H:%M:%S'),
			'lasttime': self.lasttime.strftime('%Y-%m-%d %H:%M:%S'),
			'status': self.status,
			'typeid': self.typeid,
			'status_ifg': self.status_ifg
		}
		return json_data
		# return json.dumps(json_data, cls=DateEncoder)

	def __init__(self, **kwargs):
		super(Article, self).__init__(**kwargs)	
		pass
	def __repr__(self):
		return '<Article %r>' % self.title

		
class Collection(db.Model):
	__tablename__ = 'collection'
	id = db.Column(db.Integer,primary_key=True)
	name = db.Column(db.String(50),index=True)
	list_charset = db.Column(db.Integer)
	charset = db.Column(db.Integer)
	addtime = db.Column(db.DateTime,default=datetime.now)
	typeid = db.Column(db.Integer, db.ForeignKey('arttype.id'))
	clist = db.relationship('Collectionlist')
	rlist = db.relationship('Referurl')
	retype = db.Column(db.Integer,default=1)
	reurlrex = db.Column(db.String)
	reurlrex1 = db.Column(db.String)
	musthas = db.Column(db.String(100))
	nothas = db.Column(db.String(100))
	match_title = db.Column(db.String)
	match_title_retype = db.Column(db.Integer,default=1)
	trim_title = db.Column(db.String)
	match_body = db.Column(db.String)
	match_body_retype = db.Column(db.Integer,default=1)
	trim_body = db.Column(db.String)
	match_page = db.Column(db.String)
	match_page_retype = db.Column(db.Integer,default=1)

	def __init__(self, **kwargs):
		super(Collection, self).__init__(**kwargs)	
		pass
	def __repr__(self):
		return '<Collection %r>' % self.name

class Referurl(db.Model):
	__tablename__ = 'referurl'
	id = db.Column(db.Integer,primary_key=True)
	cid = db.Column(db.Integer,db.ForeignKey('collection.id'))
	url = db.Column(db.String(100))


class Collectionlist(db.Model):
	__tablename__ = 'collectionlist'
	id = db.Column(db.Integer,primary_key=True)
	url = db.Column(db.String,unique=True)
	status = db.Column(db.Integer,default=1)
	colid = db.Column(db.Integer, db.ForeignKey('collection.id'))
	collection = db.relationship('Collection', backref='colist')

	def to_json(self):
		json_data = {
			'id': self.id,
			'url': self.url,
			'status': self.status,
			'colid': self.colid,
			'name': self.collection.name,
		}
		return json_data

	def __init__(self, **kwargs):
		super(Collectionlist, self).__init__(**kwargs)	
		pass
	def __repr__(self):
		return '<Collectionlist %r>' % self.url


class Release(db.Model):
	__tablename__ = 'release'
	id = db.Column(db.Integer,primary_key=True)
	sitename = db.Column(db.String(50),index=True)
	loginurl = db.Column(db.String(50))
	username = db.Column(db.String(50))
	password = db.Column(db.String(50))
	key = db.Column(db.String(50))
	cookies = db.Column(db.String())  
	url = db.Column(db.String(100))
	typeid = db.Column(db.Integer)
	addtime = db.Column(db.DateTime,default=datetime.now)
	charset = db.Column(db.Integer)
	adddate = db.Column(db.String(20))
	child = db.relationship('Releasechild')

	def __init__(self, **kwargs):
		super(Release, self).__init__(**kwargs)	
		pass
	def __repr__(self):
		return '<Release %r>' % self.sitename

class Addcountdb(db.Model):
	__tablename__ = 'addcount'
	id 	= db.Column(db.Integer,primary_key=True)
	rid = db.Column(db.Integer)
	adddate = db.Column(db.String(20))




	def __init__(self, **kwargs):
		super(Addcountdb, self).__init__(**kwargs)	
		pass
	def __repr__(self):
		return '<Release %r>' % self.sitename
		

class Releasechild(db.Model):
	__tablename__ = 'releasechild'
	id = db.Column(db.Integer,primary_key=True)
	sitename = db.Column(db.String(50), index=True)
	loginurl = db.Column(db.String(50))
	username = db.Column(db.String(50))
	password = db.Column(db.String(50))
	key = db.Column(db.String(50))
	cookies = db.Column(db.String())
	url = db.Column(db.String(100))
	typeid = db.Column(db.Integer)
	addtime = db.Column(db.DateTime, default=datetime.now)
	charset = db.Column(db.Integer)
	pid = db.Column(db.Integer,db.ForeignKey('release.id'))

class Ifgapi(db.Model):
	__tablename__ = 'ifgapi'
	id = db.Column(db.Integer,primary_key=True)
	userid = db.Column(db.String())
	secret = db.Column(db.String())
	status = db.Column(db.Integer)
	statusdate = db.Column(db.Integer)


class DateEncoder(json.JSONEncoder):
	def default(self,obj):
		if isinstance(obj, datetime.datetime):
			return obj.strftime(DATETIME)
		elif isinstance(obj,datetime.date):
			return obj.strftime(DATE)
		elif isinstance(obj,Decimal):
			return str(obj)
		else:
			return json.JSONEncoder.default(self,obj)