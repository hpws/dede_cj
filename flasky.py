from flask_migrate import Migrate
from app import create_app,db
import datetime

app = create_app('default')
migrate = Migrate(app, db)


# @app.template_filter('showcount')
# def showcount(rid):
#   from app.myclass.addcount import Addcount
#   aaa = Addcount()
#   aaa.get(1)

if __name__ == '__main__':
    # app.run(host = '0.0.0.0',port = 5200, debug = True,threaded=True)
    
    app.run(host = '0.0.0.0',port = 5200)