
import os
basedir = os.path.abspath(os.path.dirname(__file__))

class Config :
  SECRET_KEY = os.environ.get('SECRET_KEY') or 'hafd4j32sidfigd9d9ff9hng'

  @staticmethod
  def init_app(app):
    pass

class DevelopmentConfig(Config):
  DEBUG = True
  SQLALCHEMY_DATABASE_URI = os.environ.get('DEV_DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'data.sqlite')
  UPLOAD_FOLDER = '/uploads'


config = {
    'default': DevelopmentConfig
}